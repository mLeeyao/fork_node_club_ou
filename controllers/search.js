
var Topic        = require('../proxy').Topic;
var config       = require('../config');
var EventProxy   = require('eventproxy');
exports.index = function (req, res, next) {
  var q = req.query.q;
  var page = Number(req.query.page) || 1;
  var limit = config.list_topic_count;
  var render = function (topics, pages) {
      res.render('topic/search-topics', {
          topics: topics,
          current_page: page,
          pages: pages,
          search_word:q
      });
  };

  var proxy = new EventProxy();
  proxy.assign('topics', 'pages', render);
  proxy.fail(next);

  var query = {'title': new RegExp(q, 'i')};
  var opt = {skip: (page - 1) * limit, limit: limit, sort: '-create_at'};
  Topic.getTopicsByQuery(query, opt, proxy.done('topics'));

  Topic.getCountByQuery(query, proxy.done(function (all_topics_count) {
      var pages = Math.ceil(all_topics_count / limit);
      proxy.emit('pages', pages);
  }));
};
